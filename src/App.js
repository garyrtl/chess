import React, { Component } from 'react';
import './App.css';

import Chess from './components/Chess';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Chess />
      </div>
    );
  }
}

export default App;