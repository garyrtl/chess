import React, { Component } from 'react';
import Grid from '../Grid';

class Chessboard extends Component {

  render() {
    return (
      <table className="chessboard">
      	<tbody>
    			{ this.props.board.map((ranks,i) => 
    				<tr className="rank" key={i+'rank'}>
    					{ ranks.map((grid,j) => 
    						<td className="grid" key={j+'grid'}>
    							<Grid piece={grid} position={[i,j]} from={this.props.from} to={this.props.to}/>
    						</td>
    					)}
    				</tr>
    			)}
	      </tbody>
      </table>
    );
  }
}

export default Chessboard;