import React, { Component } from 'react';
import KingBlack from '../../assests/king-black.svg';
import QueenBlack from '../../assests/queen-black.svg';
import RookBlack from '../../assests/rook-black.svg';
import BishopBlack from '../../assests/bishop-black.svg';
import KnightBlack from '../../assests/knight-black.svg';
import PawnBlack from '../../assests/pawn-black.svg';

import KingWhite from '../../assests/king-white.svg';
import QueenWhite from '../../assests/queen-white.svg';
import RookWhite from '../../assests/rook-white.svg';
import BishopWhite from '../../assests/bishop-white.svg';
import KnightWhite from '../../assests/knight-white.svg';
import PawnWhite from '../../assests/pawn-white.svg';

class Grid extends Component {

  shouldComponentUpdate(nextProps, nextState) {
    if((nextProps.position[0]===nextProps.from[0] && nextProps.position[1]===nextProps.from[1]) ||
      (nextProps.position[0]===nextProps.to[0] && nextProps.position[1]===nextProps.to[1]) || 
      (nextProps.from[0]===0 && nextProps.from[1]===0 && nextProps.to[0]===0 && nextProps.to[1]===0)
      ) {
      return true;
    }
    else {
      return false;
    }
  }

  render() {
    let piece = null;
    let styles;
    switch(this.props.piece) {
      case 'e':
        break
      case 'K':
        piece = KingBlack;
        break
      case 'Q':
        piece = QueenBlack;
        break
      case 'B':
        piece = BishopBlack;
        break
      case 'N':
        piece = KnightBlack;
        break
      case 'R':
        piece = RookBlack;
        break
      case 'P':
        piece = PawnBlack;
        break
      case 'k':
        piece = KingWhite;
        break
      case 'q':
        piece = QueenWhite;
        break
      case 'b':
        piece = BishopWhite;
        break
      case 'n':
        piece = KnightWhite;
        break
      case 'r':
        piece = RookWhite;
        break
      case 'p':
        piece = PawnWhite;
        break
      default:
        break
    }

    if(this.props.position[0]===this.props.from[0] && this.props.position[1]===this.props.from[1]) {
      let top = (this.props.to[0] - this.props.position[0])*100;
      let left = (this.props.to[1] - this.props.position[1])*100;
      styles = {
        transform: 'translate('+ left +'%, '+ top+'%)',
        zIndex: 2
      }
    }

    return (
      <img style={styles} className="piece" draggable="false" src={piece} alt=""/>
    );
  }
}

export default Grid;