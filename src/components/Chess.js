import React, { Component } from 'react';
import Chessboard from './Chessboard';

import api from '../api';

function generateNextBoard(board, from, to) {
	var nextBoard = board.slice();
	var fromPiece = board[from[0]][from[1]];
	nextBoard[from[0]][from[1]] = 'e';
	nextBoard[to[0]][to[1]] = fromPiece;
	return nextBoard;
}

class Chess extends Component {
  
  constructor() {
  	super();
  	this.state = {
  		board: [],
  		boardNumber: null,
  		loading: false,
  		rating: null,
  		playing: false,
  		from: [],
  		to: [],
  		playInterval: 1000
  	}
  	this.interval = null;
  	this.play = this.play.bind(this);
  	this.next = this.next.bind(this);
  	this.regenerate = this.regenerate.bind(this);
  	this.updatePlayInterval = this.updatePlayInterval.bind(this);
  }

  componentDidMount() {
  	api.getCurrent().then((response) => {
  		this.setState({
  			board: response.board,
  			boardNumber: response["board-number"],
  			rating: response.rating
  		});
  	});
  }

  play() {
  	let that = this;
  	that.setState({
  		playing: !this.state.playing
  	}, function(){
  		if(this.state.playing) {
  			that.interval = setInterval(function(){ 
  				that.next();
  			}, this.state.playInterval);
  		}
  		else {
  			clearInterval(that.interval);
  		}
  	});
  }

  next() {
  	let that = this;
  	if(that.state.loading===false) {
	  	that.setState({
	  		loading: true
	  	}, function(){
		  	api.getNext().then((response) => {
		  		that.setState({
		  			board: response.board,
		  			from: response.from,
		  			to: response.to
		  		}, function(){
		  			let nextBoard = generateNextBoard(response.board, response.from, response.to);
		  			setTimeout(function(){
						  that.setState({
						  	board: nextBoard,
		  					loading: false
						  });
		  			}, 500);
		  		});
		  	});
	  	});
  	}
  }

  regenerate() {
  	let that = this;
  	clearInterval(this.interval);
  		that.setState({
  			playing: false
  		}, function(){
		  	api.regenerate().then((response) => {
			  	api.getCurrent().then((response) => {
			  		this.setState({
			  			loading: false,
			  			board: response.board,
			  			boardNumber: response["board-number"],
			  			rating: response.rating,
			  			from: [0,0],
			  			to: [0,0]
			  		});
			  	});
		  	});
  		});
  }

  updatePlayInterval(event) {
  	let that = this;
  	that.play();
  	that.setState({
  		playInterval: event.target.value
  	}, function(){
  		that.play();
  	});
  }

  render() {
    return (
      <div className="chess">
        <Chessboard board={this.state.board} from={this.state.from} to={this.state.to}/>
        <div className="controls">

        	<div className="select-speed">
        		<span>Play Speed</span>
	          <select value={this.state.playInterval} onChange={this.updatePlayInterval}>
	            <option value="250">Fast</option>
	            <option value="1000">Moderate</option>
	            <option value="2000">Slow</option>
	          </select>
        	</div>

        	<button className={(this.state.playing ? 'playing': 'paused')} onClick={this.play}>
        		<div className="play-icon"></div>
        	</button>

        	<button className={'next ' + (this.state.playing ? 'disabled': '')} onClick={this.next}>
        		<div className="next-icon left"></div>
        		<div className="next-icon right"></div>
        	</button>

        	<button className="regenerate" onClick={this.regenerate}>
        		<div className="regenerate-icon">New</div>
        	</button>

        	<div className="board-info">
        		<span>Board Number: {this.state.boardNumber}</span>
        		<span>Rating: {this.state.rating}</span>
        	</div>
        </div>
      </div>
    );
  }
}

export default Chess;