let baseUrl = 'http://34.200.222.78/';

const api = {
	getCurrent() {
		return fetch(baseUrl + 'current').then((response) => response.json());
	},

	getNext() {
		return fetch(baseUrl + 'next', {
			method: 'POST',
		}).then((response) => response.json());
	},

	regenerate() {
		return fetch(baseUrl + 'regenerate', {
			method: 'POST',
		}).then((response) => response.json());
	}
}

export default api